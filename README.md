# datagateway

Reference project for accessing a PV metrics database

## Quick start
### With latest developement
```
python -m pip install git+https://gitlab.com/pod-group/datagateway.git@main#egg=datagateway
datagateway --version
datagateway --url "some-dialect://rest_of_db_access_url"
```

## Hacking
### With venv
1. [Install](https://wiki.python.org/moin/BeginnersGuide/Download) the latest version of python
1. Use [your favorte method](https://docs.python.org/3/installing/index.html) for installing python packages to install the latest versions of [build](https://pypi.org/project/build/), [installer](https://pypi.org/project/installer/), [wheel](https://pypi.org/project/wheel/), [setuptools](https://pypi.org/project/setuptools/) and [setuptools_scm](https://pypi.org/project/setuptools-scm/))
1. [Install git](https://git-scm.com/downloads) and use it to clone this repo
    ```
    git clone https://gitlab.com/pod-group/datagateway.git
    ```
1. Make and activate a new python virtual environment (by following [these instructions](https://docs.python.org/3/library/venv.html#creating-virtual-environments)
1. Look in the project.toml file here and install all the packages under the dependencies header.
1. Then from the root of this repo:
    ```
    python -m build --wheel --no-isolation --outdir .buildwork && python -m installer .buildwork/*.whl && rm -r .buildwork
    ```
1. Now you're ready to run the program:
    ```
    datagateway --version
    datagateway --url "some-dialect://rest_of_db_access_url"

    deactivate
    ```
### Linux development notes
```
python -m build --outdir .buildwork
python -m build --wheel --no-isolation --outdir .buildwork
python -m venv ~/venvs/datagateway --clear --system-site-packages --without-pip
source ~/venvs/datagateway/bin/activate
python -m installer .buildwork/*.whl
rm -r .buildwork

datagateway --version
datagateway --url "some-dialect://rest_of_db_access_url"
```
