#!/usr/bin/env python3

import dataset
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import argparse
import sys
import sqlite3  # for dumping

from setuptools_scm import get_version
from importlib.metadata import version, PackageNotFoundError

try:
    __version__ = version("datagateway")
except PackageNotFoundError:
    # package is not installed, use setuptools_scm directly
    __version__ = get_version(".")


class DataGatewayCLI(object):
    """Command line interface to a PV metrics database"""

    parser: argparse.ArgumentParser
    db_url: str
    dump_file: str
    schema_prefix: str = "org_greyltc."
    raw_schema_prefix: str = "org_greyltc_raw."
    # mode: str

    def __init__(self):
        url_default = "postgresql://"

        # modes = ["ui", "dump"]
        # mode_default = "dump"
        # mode_default = "ui"

        dump_default = ""

        self.parser = argparse.ArgumentParser(prog="datagateway", description="A PV metrics database interface")
        # self.parser.add_argument("--mode", choices=modes, default=mode_default)
        self.parser.add_argument("--url", default=url_default)
        self.parser.add_argument("--dump-sqlite3", default=dump_default)
        self.parser.add_argument("--version", action="version", version=f"%(prog)s {__version__}")

    def parse_args(self) -> None:
        args = self.parser.parse_args()
        self.db_url = args.url
        # self.mode = args.mode
        self.dump_file = args.dump_sqlite3
        return None

    def run(self) -> int:
        ret = 0

        schema_kwarg = {"schema": self.schema_prefix[:-1]}
        if "sqlite" in self.db_url:
            # disable schema when reading sqlite db
            schema_kwarg = {}
            self.schema_prefix = ""
            self.raw_schema_prefix = ""

        execution_options = {}
        execution_options["postgresql_readonly"] = True
        db = dataset.connect(self.db_url, engine_kwargs={"execution_options": execution_options}, **schema_kwarg)
        db.ensure_schema = False

        if self.dump_file != "":  # we're dumping postgresql -> sqlite3
            tool_tables = [f's{row["hash"]}' for row in db["tbl_data_tools"]]  # get tool hashes
            sqlite_con = sqlite3.connect(self.dump_file)  # connect to the dump file
            with sqlite_con:
                for table in db.tables + tool_tables:
                    print(f"Dumping {table}...", end="")
                    sys.stdout.flush()
                    if table in tool_tables:
                        schema_kwarg["schema"] = self.raw_schema_prefix[:-1]
                    else:
                        schema_kwarg["schema"] = self.schema_prefix[:-1]
                    df = pd.read_sql_table(table, db.executable, **schema_kwarg)
                    if "isweep" in table:
                        # sqlite can't handle arrays, so just stringify the isetpoints array
                        df["isetpoints"] = pd.Series(df["isetpoints"], dtype="string")
                    df.to_sql(table, sqlite_con)
                    print("Done!")
            sqlite_con.close()
        else:  # we're running in normal mode
            user_df = pd.read_sql_table("tbl_users", db.executable, columns=["id", "name"], **schema_kwarg)
            print("=== Table of Users ===\n" + user_df.to_markdown(tablefmt="grid", index=False))
            uid = int(input("Select user id: "))
            user_name = user_df[user_df["id"] == uid]["name"].values[0]

            run_df = pd.read_sql_query(f"SELECT id, ts, name FROM {self.schema_prefix}tbl_runs WHERE user_id = {uid}", db.executable)
            print(f"\n=== Table of Runs by {user_name} ===\n" + run_df.to_markdown(tablefmt="grid", index=False))
            rid = int(input("Select run id: "))
            run_name = run_df[run_df["id"] == rid]["name"].values[0]

            event_df = pd.read_sql_query(f"SELECT id, kind, hash FROM {self.schema_prefix}tbl_events WHERE run_id = {rid}", db.executable)
            print(f"\n=== Table of Events in run {run_name} ===\n" + event_df[["id", "kind"]].to_markdown(tablefmt="grid", index=False))
            eid = int(input("Select event id: "))
            event_kind = event_df[event_df["id"] == eid]["kind"].values[0]
            raw_tbl = "s" + event_df[event_df["id"] == eid]["hash"].values[0]

            event_details = pd.read_sql_query(f"SELECT * FROM {self.schema_prefix}tbl_{event_kind}_events WHERE id = {eid}", db.executable)
            # TODO: filter this
            print(f"\n=== Details of event id {eid} ===\n" + event_details.to_markdown(tablefmt="grid", index=False))

            raw_data = pd.read_sql_query(f"SELECT * FROM {self.raw_schema_prefix}{raw_tbl} WHERE event_id = {eid}", db.executable)
            if event_kind == "sweep":
                sp = raw_data.plot(x="v", y="i", grid=True, xlabel="Sense Voltage [V]", ylabel="Current [A]")
            else:
                sp = raw_data.plot(x="t", y=["v", "i"], grid=True, xlabel="Time [s]")

            # dump raw data
            # print(f"\n=== Raw data from event id {eid} ===\n" + raw_data.to_markdown(tablefmt="grid", index=False))

            plt.show()

        return ret


def main():
    dgc = DataGatewayCLI()
    dgc.parse_args()
    return dgc.run()


if __name__ == "__main__":
    sys.exit(main())
